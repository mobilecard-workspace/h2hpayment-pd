<%@ page contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>
<html>
    <head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="Pragma" content="no-cache" />
		<meta http-equiv="expires" content="-1" />
		<meta name="HandheldFriendly" content="true" />
		<meta name="viewport" content="width=device-width" />
		
		<%	
		   	response.setHeader("Expires","0");
		   	response.setHeader("Pragma","no-cache");
		   	response.setDateHeader("Expires",-1);
		 	
		%>
		
	</head>
		
<input type="hidden" name="name" value="${prosa.nombre}" />
<input type="hidden" name="number" value="${prosa.TDC}" />
<input type="hidden" name="type" value="${prosa.tipoTDC}" />
<input type="hidden" name="expmonth" value="${prosa.mes}" />
<input type="hidden" name="expyear" value="${prosa.anio}" />
<input type="hidden" name="ccnumberback" value="${prosa.cc_numberback}" />
<input type="hidden" name="cc_cvv" value="${prosa.cc_cvv}" />

	<div id="3ds-prosa">
		
		<c:import var="testHtml"
			url="https://www.procom.prosa.com.mx/eMerchant/8039159_imdm.jsp"/>						
		<c:out value="${testHtml}" escapeXml="false" />
		
				
		<script type="text/javascript">

		
		var frm = document.getElementsByTagName('form')[0];
		//alert(frm.innerHTML);
		frm.innerHTML = frm.innerHTML+
		'<input type="hidden" name="total" value="${prosa.total}" />'+
		'<input type="hidden" name="currency" value="${prosa.currency}"/>'+
		'<input type="hidden" name="address" value="${prosa.address}"/>'+
		'<input type="hidden" name="order_id" value="${prosa.orderId}" />'+
		'<input type="hidden" name="merchant" value="${prosa.merchant}"/>'+
		'<input type="hidden" name="store" value="${prosa.store}"/>'+
		'<input type="hidden" name="term" value="${prosa.term}"/>'+
		'<input type="hidden" name="digest" value="${prosa.digest}" />'+
		'<input type="hidden" name="return_target" value="_self" />'+		
		'<input type="hidden" name="urlBack" value="${prosa.urlBack}" />'
		;
		//alert(frm.innerHTML); https://www.procom.prosa.com.mx
				
		frm.setAttribute('action','https://www.procom.prosa.com.mx/eMerchant/validaciones/valida.do');
		
		//alert(frm.getAttribute('action'));
		
		//alert(document.getElementsByName('cc_name').value);
		//alert(document.getElementsByName('cc_name')[0].parentElement);
		
		//Ocultar campos de entrada		
		/*document.getElementsByName('cc_name')[0].style.visibility="hidden"
		document.getElementsByName('cc_number')[0].style.visibility="hidden"
		document.getElementsByName('cc_type')[0].style.visibility="hidden"
		document.getElementsByName('_cc_expmonth')[0].style.visibility="hidden"
		document.getElementsByName('_cc_expyear')[0].style.visibility="hidden"*/
		
		//Establecer valores
		document.getElementsByName('cc_name')[0].value = document.getElementsByName('name')[0].value;
		document.getElementsByName('cc_number')[0].value = document.getElementsByName('number')[0].value;
		document.getElementsByName('cc_type')[0].value = document.getElementsByName('type')[0].value;
		document.getElementsByName('_cc_expmonth')[0].value = document.getElementsByName('expmonth')[0].value;
		document.getElementsByName('_cc_expyear')[0].value = document.getElementsByName('expyear')[0].value;
		//document.getElementsByName('cc_numberback')[0].value = document.getElementsByName('ccnumberback')[0].value;
		document.getElementsByName('cc_cvv2')[0].value = document.getElementsByName('cc_cvv')[0].value;
		//Eliminar renglones con las etiquetas de los campos que se ocultarón
		/*var tbl = document.getElementsByTagName('table')[0];
		tbl.deleteRow(0);
		tbl.deleteRow(1);
		tbl.deleteRow(2);
		tbl.deleteRow(3);
		tbl.deleteRow(4);*/
		
		//alert(document.getElementsByName('cc_name')[0].value);
		//alert(document.getElementsByName('number')[0].value);
		//alert(document.getElementsByName('cc_number')[0].value);
			
		//frm.submit();
		</script>
	</div>
</html>
