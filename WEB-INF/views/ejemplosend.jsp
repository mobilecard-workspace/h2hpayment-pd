<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<c:choose>
	<c:when test="${idApp == 1}">
		<style type="text/css">
 * {
/*with these codes padding and border does not increase it's width.Gives intuitive style.*/
  -webkit-box-sizing: border-box;   
  -moz-box-sizing: border-box;
  box-sizing: border-box;
  color:#33313;
  font-family: "Arial", Helvetica, sans-serif;
}

body {
   margin:0;
   padding:0;		
   color: #616161;  
   background-color:#e0e0e0;
}
div#envelope{
	width:90%;
	margin: 16px auto 0 auto;
	background-color:#ffffff;
	padding:10px 0;
	/** border:1px solid gray; **/
	border-radius:10px;
} 
form{
	width:80%;
	margin:0 10%;
}  
form header {
  text-align:center;
  font-family: 'Roboto Slab', serif;
}
/* Makes responsive fields.Sets size and field alignment.*/
input[type=text],input[type=password]{
	margin-bottom: 20px;
	margin-top: 10px;
	width:100%;
	padding: 15px;
	border-radius:5px;
	border:1px solid #dbd9d6;
	font-size: 110%;
}
input[type=submit]
{
	margin-bottom: 20px;
	width:100%;
	color:#ffffff;
	padding: 15px;
	border-radius:5px;
	background-color:#f57c00;
	font-weight: bold;
	font-size: 150%;
	height: 55px;
}
input[type=button]
{
	margin-bottom: 20px;
	width:100%;
	padding: 15px;
	color:#ffffff;
	border-radius:5px;
	background-color:#f57c00;
	font-weight: bold;
	font-size: 150%;
}
textarea{
	width:100%;
	padding: 15px;
	margin-top: 10px;
	border:1px solid #7ac9b7;
	border-radius:5px; 
	margin-bottom: 20px;
	resize:none;
}
input[type=text]:focus, textarea:focus, select:focus {
  border-color: #333132;
}
.styled-select {
	width:100%;
	overflow: hidden;
	background: #FFFFFF;
	border-radius:5px;
	border:1px solid #dbd9d6;
}
.styled-select select {
	font-size: 110%;
	width: 100%;
	border: 0 !important;
	padding: 15px 0px 15px 0px;
	-webkit-appearance: none;
	-moz-appearance: none;
	appearance: none;
	text-indent:  0.01px;
	text-overflow: '';
}
.btn-style {
    font-family: arial;
    font-weight: bold;
    width: 100%;
    /** opacity: 0.5; **/
    border: solid 1px #e6e6e6;
    border-radius: 3px;
    moz-border-radius: 3px;
    font-size: 16px;
    padding: 1px 17px;
    background-color: #f57c00;
    height: 55px;
    -webkit-appearance: none;
}
table{
	width:80%;
	margin:0 10%;
}  
table header {
	text-align:center;
	font-family: 'Roboto Slab', serif;
}
		</style>
	</c:when>
	<c:when test="${idApp == 2}">
		<style type="text/css">
 * {
/*with these codes padding and border does not increase it's width.Gives intuitive style.*/
  -webkit-box-sizing: border-box;   
  -moz-box-sizing: border-box;
  box-sizing: border-box;
  color:#33313;
  font-family: "Arial", Helvetica, sans-serif;
}

body {
   margin:0;
   padding:0;		
   color: #616161;  
   background-color:#e0e0e0;
}
div#envelope{
	width:90%;
	margin: 16px auto 0 auto;
	background-color:#ffffff;
	padding:10px 0;
	/** border:1px solid gray; **/
	border-radius:10px;
} 
form{
	width:80%;
	margin:0 10%;
}  
form header {
  text-align:center;
  font-family: 'Roboto Slab', serif;
}
/* Makes responsive fields.Sets size and field alignment.*/
input[type=text],input[type=password]{
	margin-bottom: 20px;
	margin-top: 10px;
	width:100%;
	padding: 15px;
	border-radius:5px;
	border:1px solid #dbd9d6;
	font-size: 110%;
}
input[type=submit]
{
	margin-bottom: 20px;
	width:100%;
	color:#ffffff;
	padding: 15px;
	border-radius:5px;
	background-color:#388e3c;
	font-weight: bold;
	font-size: 150%;
	height: 55px;
}
input[type=button]
{
	margin-bottom: 20px;
	width:100%;
	padding: 15px;
	color:#ffffff;
	border-radius:5px;
	background-color:#388e3c;
	font-weight: bold;
	font-size: 150%;
}
textarea{
	width:100%;
	padding: 15px;
	margin-top: 10px;
	border:1px solid #7ac9b7;
	border-radius:5px; 
	margin-bottom: 20px;
	resize:none;
}
input[type=text]:focus, textarea:focus, select:focus {
  border-color: #333132;
}
.styled-select {
	width:100%;
	overflow: hidden;
	background: #FFFFFF;
	border-radius:5px;
	border:1px solid #dbd9d6;
}
.styled-select select {
	font-size: 110%;
	width: 100%;
	border: 0 !important;
	padding: 15px 0px 15px 0px;
	-webkit-appearance: none;
	-moz-appearance: none;
	appearance: none;
	text-indent:  0.01px;
	text-overflow: '';
}
.btn-style {
    font-family: arial;
    font-weight: bold;
    width: 100%;
    /** opacity: 0.5; **/
    border: solid 1px #e6e6e6;
    border-radius: 3px;
    moz-border-radius: 3px;
    font-size: 16px;
    padding: 1px 17px;
    background-color: #388e3c;
    height: 55px;
}
table{
	width:80%;
	margin:0 10%;
}  
table header {
	text-align:center;
	font-family: 'Roboto Slab', serif;
}
		</style>
	</c:when>
</c:choose>
</head>
<body>
<form method="post" name="form1" action="199.231.161.38:8081/MCH2HPayment/enqueuePayment">
            <input type="text" name="idUser" value="8923233103984"/>
            <input type="text" name="idCard" value="446"/>
            <input type="text" name="accountId" value="12"/>
            <input type="text" name="amount" value="1.0"/>
            <input type="text" name="concept" value="transaferencia 7"/> 
            <input type="text" name="idioma" value="es"/>  
			<input type="submit" value="Submit">                        
        </form>
</body>
</html>