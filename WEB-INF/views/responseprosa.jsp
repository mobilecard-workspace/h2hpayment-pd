<%-- 
    Document   : comercio_fin
    Created on : 15/03/2016
    Author     : RHT
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
    <head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="Pragma" content="no-cache" />
		<meta http-equiv="expires" content="-1" />
		<meta name="HandheldFriendly" content="true" />
		<meta name="viewport" content="width=device-width" />
		
		<%	
		   	response.setHeader("Expires","0");
		   	response.setHeader("Pragma","no-cache");
		   	response.setDateHeader("Expires",-1);
		 	
		%>
        <title>Procesando</title>
        
         <!-- CSS -->
    	<link href="data:image/x-icon;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAC/0lEQVQ4T6XS+0vVdxzH8ef78z16cq4dSIaUTcpLF/xhYadyy6ytuTLyB81LjaK8IBVESyiYxCLapW3RyLXGzrpYCt0oTjWXTbLOmpY5o2Oji9HUI5pLtvJCl3M83/cg+w/2+uX12+Onp4TqK1X/akIcYOaswqQsQX27oK8NxIC7GJk8D/UfR1ur0OfDyJQsZH45EhmFhGq/1PDZT5BIsJLfxZScg13JYIchyoUUVKM9rdCwA6Jj0ecDMD0X/bsds7YOsf8NaHBrPFZMLBLsx6SthTtnEIcTXHGw+jzsnQFvpUH+EbTnBtrswW7yYMrqR4GRbfEYdz6m/xYM9yHuIui4BNExkP09HFkC8enw4U703i/Qdwv7yh4ku/IVsD0ea1YhMukduPQ5UnoRvCWjQMEJOJAO490w2Ac9f6CpJajvK2R1LTJycbfqz+WY8UlI3kFkqBec0dCwdfSzvoPA79B7AxIXw+NO9NkgdF+Dknpk5MIXSqcPE2FB5nYkbia0HYWADyIiIGU5xKXB7VNw/wK8eIqOS4b0TTDGhYQ8OUr7WayYiWAEyauC8xvB6YTEhdD7G8xYDykfwfUfwLcTtaJh+BEsqEDCbV61awqwVhyGXyuQwhq46wUBZhZD7RoorAfnWBjoBs9cmLsFfWMCOKKQsN+rdnU+1rJKuP4jsvBT6G4C0VHAuxJWXYZAI4ydCIezIHEROrsMjAMJHd+gNO/DuJcjHQ2QsRm5fRocDpi6FPyHICETOq/A9Dxo3INOykAftoG7FAm3+1Qv70ZedyFJ85E3p8HNarAckLoG/DWgNi9bT8lDW/bDSBB1upB5HyPBqiJloAsTCTI1E3ltHLR4kOAQbLgJJ1dCzBToboZZZWhjJSQswA60QuzbyItv3lcroxQzeTYQhobPYOghPPoTKaqDq3vhSRf6uAtWHIO6CpizDh3sx/afQUK+nzR8chMyJgJH7tdobTkmd9/LIkl8Dzp8kPQB/PMAJqRiN+8HVwL2k36snG8RVVX+x/4D734yc+k8tqYAAAAASUVORK5CYII=" rel="icon" type="image/x-icon" />
      
        <script type="text/javascript">
            function sendform() {
            	move();
            	document.form1.submit();
            }
            
            function move() {
            	  var elem = document.getElementById("myBar");
            	  var width = 10;
            	  var id = setInterval(frame, 10);
            	  function frame() {
            	    if (width >= 100) {
            	      clearInterval(id);
            	    } else {
            	      width++;
            	      elem.style.width = width + '%';
            	      document.getElementById("label").innerHTML = width * 1  + '%';
            	    }
            	  }
            	}
        </script>
        
        <style>
        root {
	display: block;
}

html {
	margin: 0;
	font-size: 62.5%;
	color: white;
	background-color: #D3441C;
}
body {
	font-size: 14px;
    font-weight: bold;
	line-height: 1.428571429;
}

@media ( min-width :750px) {
	.container {
		width: 100%;
		margin-right: auto;
		margin-left: auto;
	}
}

@media ( min-width :992px) {
	.container {
		width: 700px;
		margin-right: auto;
		margin-left: auto;
	}
} 
#myProgress {
  position: relative;
  width: 100%;
  height: 30px;
  background-color: #ddd;
}

#myBar {
  position: absolute;
  width: 10%;
  height: 100%;
  background-color: #4CAF50;
}

#label {
  text-align: center;
  line-height: 30px;
  color: white;
}
</style>

<!-- <style type="text/css">
root {
	display: block;
}

html {
	margin: 0;
	font-size: 62.5%;
	color: white;
	background-color: #D3441C;
}
body {
	font-size: 14px;
    font-weight: bold;
	line-height: 1.428571429;
}

@media ( min-width :750px) {
	.container {
		width: 100%;
		margin-right: auto;
		margin-left: auto;
	}
}

@media ( min-width :992px) {
	.container {
		width: 700px;
		margin-right: auto;
		margin-left: auto;
	}
} 
</style>  -->
    </head>
    <body ><!-- onload="sendform();" sendform(); -->
	
	 <!-- Contenido -->
    
      <div class="container">
      	<form method="post" name="form1" action="${url_response}">
            <input type="hidden" name="EM_Response" value="${EM_Response}">
            <input type="hidden" name="EM_RefNum" value="${EM_RefNum}">
            <input type="hidden" name="EM_Auth" value="${EM_Auth}">
            <input type="hidden" name="monto" value="${total}">
            <input type="hidden" name="referenciaSCT" value="${referenciaSCT}"> 
            <input type="hidden" name="descripcionSCT" value="${descripcionSCT}">                          
        </form>
        <h2>PROCESANDO PAGO...</h2>
        <div id="myProgress">
		  <div id="myBar">
		    <div id="label">10%</div>
		  </div>
		</div>
        <div class="container">
		<br />
	</div>
      </div>
   
    </body>
</html>
