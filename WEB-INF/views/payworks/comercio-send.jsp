<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html>

<head>
<title>Purchase Verification</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="expires" content="-1" />
<meta name="HandheldFriendly" content="true" />
<meta name="viewport" content="width=device-width" />
<c:choose>
	<c:when test="${idApp == 1}">
		<style type="text/css">
 * {
/*with these codes padding and border does not increase it's width.Gives intuitive style.*/
  -webkit-box-sizing: border-box;   
  -moz-box-sizing: border-box;
  box-sizing: border-box;
  color:#33313;
  font-family: "Arial", Helvetica, sans-serif;
}

body {
   margin:0;
   padding:0;		
   color: #616161;  
   background-color:#e0e0e0;
}
div#envelope{
	width:90%;
	margin: 16px auto 0 auto;
	background-color:#ffffff;
	padding:10px 0;
	/** border:1px solid gray; **/
	border-radius:10px;
} 
form{
	width:80%;
	margin:0 10%;
}  
form header {
  text-align:center;
  font-family: 'Roboto Slab', serif;
}
/* Makes responsive fields.Sets size and field alignment.*/
input[type=text],input[type=password]{
	margin-bottom: 20px;
	margin-top: 10px;
	width:100%;
	padding: 15px;
	border-radius:5px;
	border:1px solid #dbd9d6;
	font-size: 110%;
}
input[type=submit]
{
	margin-bottom: 20px;
	width:100%;
	color:#ffffff;
	padding: 15px;
	border-radius:5px;
	background-color:#f57c00;
	font-weight: bold;
	font-size: 150%;
	height: 55px;
}
input[type=button]
{
	margin-bottom: 20px;
	width:100%;
	padding: 15px;
	color:#ffffff;
	border-radius:5px;
	background-color:#f57c00;
	font-weight: bold;
	font-size: 150%;
}
textarea{
	width:100%;
	padding: 15px;
	margin-top: 10px;
	border:1px solid #7ac9b7;
	border-radius:5px; 
	margin-bottom: 20px;
	resize:none;
}
input[type=text]:focus, textarea:focus, select:focus {
  border-color: #333132;
}
.styled-select {
	width:100%;
	overflow: hidden;
	background: #FFFFFF;
	border-radius:5px;
	border:1px solid #dbd9d6;
}
.styled-select select {
	font-size: 110%;
	width: 100%;
	border: 0 !important;
	padding: 15px 0px 15px 0px;
	-webkit-appearance: none;
	-moz-appearance: none;
	appearance: none;
	text-indent:  0.01px;
	text-overflow: '';
}
.btn-style {
    font-family: arial;
    font-weight: bold;
    width: 100%;
    /** opacity: 0.5; **/
    border: solid 1px #e6e6e6;
    border-radius: 3px;
    moz-border-radius: 3px;
    font-size: 16px;
    padding: 1px 17px;
    background-color: #f57c00;
    height: 55px;
    -webkit-appearance: none;
}
table{
	width:80%;
	margin:0 10%;
}  
table header {
	text-align:center;
	font-family: 'Roboto Slab', serif;
}
		</style>
	</c:when>
	<c:when test="${idApp == 2}">
		<style type="text/css">
 * {
/*with these codes padding and border does not increase it's width.Gives intuitive style.*/
  -webkit-box-sizing: border-box;   
  -moz-box-sizing: border-box;
  box-sizing: border-box;
  color:#33313;
  font-family: "Arial", Helvetica, sans-serif;
}

body {
   margin:0;
   padding:0;		
   color: #616161;  
   background-color:#e0e0e0;
}
div#envelope{
	width:90%;
	margin: 16px auto 0 auto;
	background-color:#ffffff;
	padding:10px 0;
	/** border:1px solid gray; **/
	border-radius:10px;
} 
form{
	width:80%;
	margin:0 10%;
}  
form header {
  text-align:center;
  font-family: 'Roboto Slab', serif;
}
/* Makes responsive fields.Sets size and field alignment.*/
input[type=text],input[type=password]{
	margin-bottom: 20px;
	margin-top: 10px;
	width:100%;
	padding: 15px;
	border-radius:5px;
	border:1px solid #dbd9d6;
	font-size: 110%;
}
input[type=submit]
{
	margin-bottom: 20px;
	width:100%;
	color:#ffffff;
	padding: 15px;
	border-radius:5px;
	background-color:#388e3c;
	font-weight: bold;
	font-size: 150%;
	height: 55px;
}
input[type=button]
{
	margin-bottom: 20px;
	width:100%;
	padding: 15px;
	color:#ffffff;
	border-radius:5px;
	background-color:#388e3c;
	font-weight: bold;
	font-size: 150%;
}
textarea{
	width:100%;
	padding: 15px;
	margin-top: 10px;
	border:1px solid #7ac9b7;
	border-radius:5px; 
	margin-bottom: 20px;
	resize:none;
}
input[type=text]:focus, textarea:focus, select:focus {
  border-color: #333132;
}
.styled-select {
	width:100%;
	overflow: hidden;
	background: #FFFFFF;
	border-radius:5px;
	border:1px solid #dbd9d6;
}
.styled-select select {
	font-size: 110%;
	width: 100%;
	border: 0 !important;
	padding: 15px 0px 15px 0px;
	-webkit-appearance: none;
	-moz-appearance: none;
	appearance: none;
	text-indent:  0.01px;
	text-overflow: '';
}
.btn-style {
    font-family: arial;
    font-weight: bold;
    width: 100%;
    /** opacity: 0.5; **/
    border: solid 1px #e6e6e6;
    border-radius: 3px;
    moz-border-radius: 3px;
    font-size: 16px;
    padding: 1px 17px;
    background-color: #388e3c;
    height: 55px;
}
table{
	width:80%;
	margin:0 10%;
}  
table header {
	text-align:center;
	font-family: 'Roboto Slab', serif;
}
		</style>
	</c:when>
</c:choose>
<!-- 
<style type="text/css">
root {
	display: block;
}

html {
	margin: 0;
	font-size: 62.5%;
}
body {
	font-size: 14px;
    font-weight: bold;
	line-height: 1.428571429;
}

@media ( min-width :750px) {
	.container {
		width: 100%;
		margin-right: auto;
		margin-left: auto;
	}
}

@media ( min-width :992px) {
	.container {
		width: 700px;
		margin-right: auto;
		margin-left: auto;
	}
} 
</style> -->
</head>
<body style="width: 100%">
<form:form name="form1" autocomplete="off" action="https://eps.banorte.com/secure3d/Solucion3DSecure.htm" modelAttribute="payworks">
<form:hidden path="Reference3D"/>
<form:hidden path="MerchantId"/>
<input type="hidden" name="MerchantName" value="H2H" />
<input type="hidden" name="MerchantCity" value="Monterrey" />
<input type="hidden" name="Cert3D" value="03" />
<input type="hidden" name="NoFrame" value="True" />
<form:hidden path="ForwardPath"/>
<form:hidden path="Expires"/>
<form:hidden path="Total"/>
<form:hidden path="Card"/>
<form:hidden path="CardType"/>
</form:form>

	<div class="container">
		
		<br />
		<table border="0" width="100%">
			<thead>
			<tr>
				<th><p>Envio de informaci&oacute;n.</p></th>
			</tr>
			</thead>
			<tbody>
			<tr>
				<td>
					<p><br/>Se est&aacute; enviando la informaci&oacute;n, favor de esperar. <br/>
					</p>
				</td>
			</tr>
			</tbody>
		</table>
	</div>
	<script type="text/javascript">
		document.form1.submit();
	</script>
</body>
</html>
